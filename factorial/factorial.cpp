#include <iostream>

using namespace std;

int recursion(int unumber){
    if (unumber > 1){
        return unumber * recursion(unumber-1);
    } else {
        return 1;
    }
}

int main ()
{
    int number ;
    cout << " Enter a number : ";
    cin >> number ;
    if (number > 0){
    cout << "The factorial of " << number << " is " << recursion(number);
    } else {
        cout << "This is not a valid number";
    }
    
/*
    cout << "The factorial of " << number << " is ";
    int accumulator = 1;
    for (; number > 0; accumulator *= number --)
        cout << accumulator << ".\n";*/
    return 0;
}