#include <iostream>

using namespace std;

int max(int array[], int size){
    int max = 0;
    for (int i = 0; i < size; i++){
        if (array[i] > max){
            max = array[i];
        }
    }
    return max;
}

int min(int array[], int size){
    int min = array[0];
    for (int i = 0; i < size; i++){
        if (array[i] < min){
            min = array[i];
        }
    }
    return min;
}

int average(int array[], int size){
    int sum = 0;
    int average = 0;
    for (int i = 0; i < size; sum += array[i++]);
    average = sum / size;
    return average;
    
}

int range(int array[], int size){
    int range = 0;
    range = max(array, size) - min(array, size);
    return range;
}

int main(){
    int nb = 0;
    int i = 0;

    cout << "Enter how much numbers you want : ";
    cin >> nb;
    cout << endl;
    int narray[nb];
    //cout << sizeof(narray) / sizeof(int) << endl;

    cout << "Enter your numbers :" << endl;
    for (i = 0; i < nb;){
        cin >> narray[i];
        i++;
    }

    cout << "Maximum value is : " << max(narray, nb) << endl;
    cout << "Minimum value is : " << min(narray, nb) << endl;
    cout << "Average value is : " << average(narray, nb) << endl;
    cout << "Range value is : " << range(narray, nb) << endl;




    /*cout << "Your array is : ";
    for (int j = 0; j < nb;){
        cout << narray[j] << " ";
        j++;
    }*/

    return 0;
}