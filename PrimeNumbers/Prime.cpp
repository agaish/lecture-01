#include <iostream>

using namespace std;

int main(){
    bool prime = true;
    int number = 0;

    cout << "Enter a number" << endl;
    cin >> number;

    if (number <= 1){
        prime = false;
    }

        for (int i = 2; i <= number/2; ++i){
            if(number % i == 0){
                prime = false;
            }
        }
    
    if (prime) cout << number << " is prime number" << endl;
    else cout << number << " is not prime number" << endl;

    return 0;
    
}